package test;

import test.FileToStr;
import test.wordcount;
import java.io.IOException;
import java.util.*;

public class main {
	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		String file = sc.next();
		String wordLong = sc.next();
		FileToStr fd = new FileToStr();
		String text = fd.FileToString(file);
		String str = new String();
		wordcount wt = new wordcount(text);
		wt.getCharacterCount(); // 字符、空格、制表、换行统计
		wt.getWordCount(); // 单词统计
		wt.getLineValidate(); // 有效行数统计
		System.out.println(wt.text);
		wt.countLong(file);
		str = "字符数=" + wt.charCount+"\n空格数=" + wt.blankCount+"\n水平字符数=" + wt.tabCount+"\n换行符数=" 
		+ wt.enterCount+"\n均算字符数=" + wt.total+"\n行数=" + wt.lineCount+"\n单词数=" + wt.getWordCount()+"\n有效行数=" + wt.lineValidate;
		fd.WriteToFile(str);
		System.out.println(str);
		
		sc.close();
	}
}
